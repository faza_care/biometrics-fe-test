import React from 'react';
import { Form, Button } from '@douyinfe/semi-ui';
import axios from 'axios';
import { startRegistration } from '@simplewebauthn/browser';

const SignupForm = () => {
  const signup = async (values) => {
    console.log(values)


    // fetch(`http://localhost:3006/biometrics/register/generate-options?deviceId=${values.deviceId}`,{
    //     headers:{
    //       'care-jwt-validated': true,
    //       'care-consumer': `{"careId": "${values.careId}"}`,
    //       'Access-Control-Allow-Origin': '*',
    //     }
    //   }).then(response => console.log(response))
    //   .catch(error=> console.log(error));
    try {
      const response = await axios.get(`https://api.dev.getcare.io/v1/auth/biometrics/register/generate-options?deviceId=${values.deviceId}`,{
        headers:{
          'care-jwt-validated': true,
          // 'care-consumer': `{"careId": "${values.careId}"}`,
          'Access-Control-Allow-Origin': '*',
          'Authorization': `Bearer ${values.bearer}`
        }
      })
      console.log(response)

      const data = {
        "rp": {
            "name": "Care Biometric Authentication",
            "id": "localhost"
        },
        "user": {
            "id": "MzkyZDcxYzctYWYyNC00MGUyLTgxMWUtZjNjMDA1Mzg5NmYz",
            "name": "Faza",
            "displayName": ""
        },
        "challenge": "pE_1iEoCZZz94QmePSAStuw2dhZPwjRkMpSo1kqFs_E",
        "pubKeyCredParams": [
            {
                "alg": -8,
                "type": "public-key"
            },
            {
                "alg": -7,
                "type": "public-key"
            },
            {
                "alg": -257,
                "type": "public-key"
            }
        ],
        "timeout": 300000,
        "excludeCredentials": [],
        "authenticatorSelection": {
            "authenticatorAttachment": "platform"
        },
        "attestation": "none",
        "extensions": {
            "credProps": true
        }
    }
      console.log(data)

      const attResp = await startRegistration(data);

      console.log(JSON.stringify(attResp))
      const ver = await axios.post('http://localhost:3006/biometrics/register/verify-options',
      {
        deviceId: values.deviceId,
        option: attResp
      },
      {
        headers:{
          'care-jwt-validated': true,
          // 'care-consumer': `{"careId": "${values.careId}"}`,
          'Access-Control-Allow-Origin': '*',
          'Authorization': `Bearer ${values.bearer}`
        }
      })

      console.log(ver)

    } catch(ex) {
      console.log('es')
      console.log(ex)
    }
    
  }

  return (
    <div>
      <div className="signup_container" style={{ 
        backgroundColor: 'antiquewhite',
        height: 300,
        width: 300,
        }}>
          <Form className="signup_form" style={{ 
          backgroundColor: 'antiquewhite',
          height: 200,
          width: 200,
          marginLeft: 40,
          }}
          onSubmit={values => signup(values)}
          >
              {
                ({ formState, values, formApi }) =>(
                  <>
                <Form.Input
                label={{ text: "Device Id" }}
                field="deviceId"
                placeholder="deviceId"
                style={{ width: "100%" }}
                fieldStyle={{ alignSelf: "stretch", padding: 0 }}
              />
              <Form.Input
                label={{ text: "careId" }}
                field="careId"
                placeholder="careId"
                style={{ width: "100%" }}
                fieldStyle={{ alignSelf: "stretch", padding: 0 }}
              />
               <Form.Input
                label={{ text: "bearer" }}
                field="bearer"
                placeholder="bearer"
                style={{ width: "100%" }}
                fieldStyle={{ alignSelf: "stretch", padding: 0 }}
              />
              <Button theme="solid" className="button" color='white' htmlType='submit'>
              Signup
            </Button>
              </>

            )}
            </Form>
            
            
          </div>
    </div>
  )
}

export default SignupForm;