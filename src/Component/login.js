import React from 'react';
import { Form, Button } from '@douyinfe/semi-ui';
import axios from 'axios';
import { startAuthentication } from '@simplewebauthn/browser';
import { values } from 'lodash';

const LoginForm = () => {
  const login = async (values) => {
    console.log(values)


    // fetch(`http://localhost:3006/biometrics/register/generate-options?deviceId=${values.deviceId}`,{
    //     headers:{
    //       'care-jwt-validated': true,
    //       'care-consumer': `{"careId": "${values.careId}"}`,
    //       'Access-Control-Allow-Origin': '*',
    //     }
    //   }).then(response => console.log(response))
    //   .catch(error=> console.log(error));
    try {
      // const response = await axios.get(`http://localhost:3006/biometrics/authenticate/generate-options?deviceId=${values.deviceId1}`,{
      //   headers:{
      //     'care-jwt-validated': true,
      //     'care-consumer': `{"careId": "${values.careId1}"}`,
      //     'Access-Control-Allow-Origin': '*',
      //   }
      // })
      // console.log(response)

      const data = {
        "rp": {
            "name": "Care Biometric Authentication",
            "id": "care.biometric"
        },
        "user": {
            "id": "MzkyZDcxYzctYWYyNC00MGUyLTgxMWUtZjNjMDA1Mzg5NmYz",
            "name": "Faza",
            "displayName": ""
        },
        "challenge": "nOVx2Y-IhLjAVXfhMAj9W5ySUMiy-Pw8N2pYPafpaWU",
        "pubKeyCredParams": [
            {
                "alg": -8,
                "type": "public-key"
            },
            {
                "alg": -7,
                "type": "public-key"
            },
            {
                "alg": -257,
                "type": "public-key"
            }
        ],
        "timeout": 300000,
        "excludeCredentials": [],
        "authenticatorSelection": {
            "authenticatorAttachment": "platform"
        },
        "attestation": "none",
        "extensions": {
            "credProps": true
        }
    }

      const attResp = await startAuthentication(data);
      // console.log(attResp)

      console.log(attResp)
      const ver = await axios.post('http://localhost:3006/biometrics/authenticate/verify-options',
      {
        deviceId: values.deviceId1,
        option: attResp
      },
      {
        headers:{
          'care-jwt-validated': true,
          'care-consumer': `{"careId": "${values.careId1}"}`,
          'Access-Control-Allow-Origin': '*',
        }
      })

      console.log(ver)

    } catch(ex) {
      console.log('es')
      console.log(ex)
    }
    console.log('DONEEEE!')
    
  }

  return (
    <div>
      <div className="login_container" style={{ 
        backgroundColor: 'antiquewhite',
        height: 300,
        width: 300,
        }}>
          <Form 
          // id='loginform'
          className="login_form" style={{ 
          backgroundColor: 'antiquewhite',
          height: 200,
          width: 200,
          marginLeft: 40,
          }}
          onSubmit={values => login(values)}
          >
              {
                ({ formState, values, formApi }) =>(
                  <>
                <Form.Input
                label={{ text: "Device Id" }}
                field="deviceId1"
                placeholder="deviceId"
                style={{ width: "100%" }}
                fieldStyle={{ alignSelf: "stretch", padding: 0 }}
              />
              <Form.Input
                label={{ text: "careId" }}
                field="careId1"
                placeholder="careId"
                style={{ width: "100%" }}
                fieldStyle={{ alignSelf: "stretch", padding: 0 }}
              />
              <Button 
            theme="solid" 
            className="button" 
            color='white' 
            // form='loginform'
            htmlType='submit'
            // value='Submit'
            >
              Login
            </Button>
              
              </>
            )}
            </Form>
            
            
            
          </div>
    </div>
  )
}

export default LoginForm;