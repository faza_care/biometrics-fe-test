import logo from './logo.svg';
import './App.css';
import SignupForm from './Component/signup';
import LoginForm from './Component/login';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          <div style={{
            height: 500,
            width: 500,
          }}>
            <SignupForm/>
            {/* <LoginForm/> */}
          </div>
          

        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
